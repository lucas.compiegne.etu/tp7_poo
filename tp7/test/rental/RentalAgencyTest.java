package rental;

import static org.junit.Assert.*;

import java.beans.Transient;

import org.junit.Before;
import org.junit.Test;

import rental.Vehicle;

public class RentalAgencyTest {

	private RentalAgency a1;
	private Client c1;
	private Client c2;
	private Vehicle v1;
   	private Vehicle v2;

   /* méthode exécutée avant l'exécution de chacune des méthodes de test */
   @Before
   public void before() {
		this.a1 = new RentalAgency();
		this.c1 = new Client("client1", 20);
      	this.c2 = new Client("client2", 30);
		this.v1 = new Vehicle("brand1", "model1", 2000, 100);
		this.v2 = new Vehicle("brand2", "model1", 2005, 150);

		this.a1.addVehicle(this.v1);
		this.a1.addVehicle(this.v2);
	}

	@Test
	public void twoClientObjectsWithSameNameCorrespondsToSameClient() throws IllegalStateException, UnknownVehicleException {
		RentalAgency agency = new RentalAgency();
		Vehicle v = new Vehicle("Vroum", "Vroum", 2000, 100);
		agency.addVehicle(v);
		Client client1 = new Client("Tim Oleon", 25);
		assertEquals(100.0, agency.rentVehicle(client1, v),0.0001);
		assertTrue(agency.hasRentedAVehicle(client1));
		// client2 corresponds to same client than client1 since names are equals
		Client client2 = new Client("Tim Oleon", 25);
		// then test should succeed
		assertTrue(agency.hasRentedAVehicle(client2));
	}


	@Test
	public void RentedVehicle() throws UnknownVehicleException {
		assertFalse(a1.hasRentedAVehicle(c1));
		assertFalse(a1.isRented(v1));
		a1.rentVehicle(c1, v1);
		
		assertTrue(a1.isRented(v1));
		assertTrue(a1.hasRentedAVehicle(c1));
	}
	// ---Pour permettre l'execution des tests ----------------------
	public static junit.framework.Test suite() {
		return new junit.framework.JUnit4TestAdapter(rental.RentalAgencyTest.class);
	}

}
