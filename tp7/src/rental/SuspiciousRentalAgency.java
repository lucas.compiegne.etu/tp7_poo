package rental;

public class SuspiciousRentalAgency extends RentalAgency{
    
    public SuspiciousRentalAgency(){
        super();
    }

    public float rentVehicle(Client c1, Vehicle v1) throws UnknownVehicleException, IllegalStateException {
        float res = 0;
        if(c1.getAge() < 25){
            float a = v1.getDailyPrice();
            res = a + ((a*10)/100);
            return res;
        }
        else return super.rentVehicle(c1, v1);
        
    }
}
