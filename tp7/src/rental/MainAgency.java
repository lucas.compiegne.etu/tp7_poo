package rental;


public class MainAgency {
    public static void main(String[] args) throws IllegalStateException, UnknownVehicleException {
        System.out.println("=========================== Partie 1");
        RentalAgency Agency = new RentalAgency();

        Vehicle v1 = new Vehicle("Tesla", "S",2012 , 150);
        Vehicle v2 = new Vehicle("Tesla", "3", 2017, 170);
        Vehicle v3 = new Vehicle("Tesla", "X", 2015, 160);
        Vehicle v4 = new Vehicle("Mercedes-Benz", "EQA" , 2021, 160);
        Vehicle v5 = new Vehicle("BMW", "X6", 2008, 120);

        Agency.addVehicle(v1);
        Agency.addVehicle(v2);
        Agency.addVehicle(v3);
        Agency.addVehicle(v4);
        Agency.addVehicle(v5);


        Client c1 = new Client("Lucas", 20);
        Client c2 = new Client("Youssef", 19);
        Client c3 = new Client("Joseph", 48);

        System.out.println("Vehicules de l'agence : " + Agency.getAllVehicles());

        AndFilter Filter = new AndFilter();

        BrandFilter F1 = new BrandFilter("Tesla");
        BrandFilter F2 = new BrandFilter("BMW");
        MaxPriceFilter F3 = new MaxPriceFilter(170);

        Filter.addFilter(F1);
        Filter.addFilter(F3);

        v1.toString();
        System.out.println("Vehicule 1 accepté par le filtre Tesla + prix < 170 ? " + Filter.accept(v1));
        System.out.println("Vehicule 4 accepté par le filtre Tesla + prix < 170 ? " + Filter.accept(v4));

        System.out.println("Filtre BMW : " + Agency.select(F2));
        System.out.println("Vehicules accepté par le filtre Tesla: "); 
        Agency.displaySelection(F1);

        System.out.println("Vehicules avec un daily price inférieur à 170 : " + Agency.select(F3));
        
        Agency.rentVehicle(c1, v2);
        Agency.rentVehicle(c2, v3);

        System.out.println("rented Vehicles : " + Agency.allRentedVehicles());
        
        System.out.println("Client 1 rented vehicle ? " + Agency.hasRentedAVehicle(c1));
        System.out.println("Vehicles 2 rented ? " + Agency.isRented(v2));
        System.out.println("Vehicles 5 rented ? " + Agency.isRented(v5));

        Agency.returnVehicle(c2);

        System.out.println("rented Vehicles after returning by c2 : " + Agency.allRentedVehicles());
        
        System.out.println("Client 3 rented vehicle ? " + Agency.hasRentedAVehicle(c3));


        System.out.println("\n=========================== Partie 2");

        Vehicle v6 = new Car("Tesla", "Y", 2020, 300, 4);
        Vehicle v7 = new Motorbike("Suzuki", "Hayabusa", 2020, 250, 1340);

        Agency.addVehicle(v6);
        Agency.addVehicle(v7);

        System.out.println("Vehicules de l'agence : " + Agency.getAllVehicles());

        MaxPriceFilter F4 = new MaxPriceFilter(270);
        System.out.println("Vehicules avec un daily price inférieur à 270 : " + Agency.select(F4));

        System.out.println("\n=========================== Partie 3");

        SuspiciousRentalAgency Agency2 = new SuspiciousRentalAgency();
        Agency2.addVehicle(v1);
        Agency2.addVehicle(v2);
        Agency2.addVehicle(v3);
        Agency2.addVehicle(v4);

        Client c4 = new Client("Client 3", 25);
        System.out.println("Le client1 a 20ans donc une surtaxe de 10% est ajouté (de base 150) : " + Agency2.rentVehicle(c1, v1));
        System.out.println("Le client2 a 19ans donc une surtaxe de 10% est ajouté (de base 160) : " + Agency2.rentVehicle(c1, v3));
        System.out.println("Le client3 a 48ans donc pas de surtaxe (de base 170) : " + Agency2.rentVehicle(c3, v2));
        System.out.println("Le client4 a 25ans donc pas de surtaxe (de base 160) : " + Agency2.rentVehicle(c4, v4));
    }
}
