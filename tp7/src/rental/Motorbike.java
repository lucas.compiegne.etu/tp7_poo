package rental;

public class Motorbike extends Vehicle {

    private float cylindrée;

    public Motorbike(String brand, String model, int productionYear, float dailyRentalPrice, float cylindre) {
        super(brand, model, productionYear, dailyRentalPrice);
        this.cylindrée = cylindre;
    }

    public String toString(){
        return super.toString() + " et son cylindrée est de : " + this.cylindrée + " cm3";
    }
    
}
