package rental;

public class Car extends Vehicle{

    private int nbPassenger;

    public Car(String brand, String model, int productionYear, float dailyRentalPrice, int NbPassenger) {
        super(brand, model, productionYear, dailyRentalPrice);
        this.nbPassenger = NbPassenger;

    }
    
    public String toString(){
        return super.toString() + " et le nombre de passager est de : " + this.nbPassenger;
    }
}
