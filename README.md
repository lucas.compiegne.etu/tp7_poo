Compiègne Lucas
Mikou Youssef

Objectif du TP : nous apprendre à gérer l'héritage et les Tables (maps et listes)

//

Générer la documentation : javadoc -sourcepath src -d docs -subpackages rental

//

Compiler les sources du projets : javac -sourcepath src -d classes src/rental/*.java

//

Compiler les tests : javac -classpath test4poo.jar test/rental/*.java

//

Exécution du main : java -classpath classes rental.MainAgency

//

Exécution des tests : 

java -jar test4poo.jar rental.BrandFilterTest

java -jar test4poo.jar rental.ClientTest

java -jar test4poo.jar rental.MaxPriceFilterTest

java -jar test4poo.jar rental.RentalAgencyTest

java -jar test4poo.jar rental.VehicleTest


//

Génération du .jar : jar cvfe appli.jar rental.MainAgency -C classes .

Exécution du .jar : java -jar appli.jar
